﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaltzProj
{
    class Order
    {
        private readonly DateTime time_of_making_order;
        public DateTime Time_of_making_order { get; }
        public List<FoodItem> foodItems;
        public double Total { get; set; }
        public DateTime Time_of_customer_to_get_order { get; set; }
        public string Customer_Address { get; set; }
        public Order(string customer_Address)
        {
            foodItems = new List<FoodItem>();
            time_of_making_order = DateTime.Now;
            Time_of_making_order = DateTime.Now;
            Customer_Address = customer_Address;
        }
        public static Order operator + (Order order, FoodItem foodItem)
        {
            order.foodItems.Add(foodItem);
            order.Total += foodItem.Price;
            return order;
        }
        public static bool operator > (Order order, DateTime time)
        {
            if (order.Time_of_customer_to_get_order.Hour != time.Hour)
                return order.Time_of_customer_to_get_order.Hour > time.Hour;
            return order.Time_of_customer_to_get_order.Minute > time.Minute;
        }
        public static bool operator <(Order order, DateTime time)
        {
            return !(order > time);
        }
    }
}
