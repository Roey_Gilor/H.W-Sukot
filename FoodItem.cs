﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaltzProj
{
    class FoodItem
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public bool Food_Or_Drink { get; set; }
        public double Capacity { get; set; }
        public FoodItem(string name, double price, bool food_or_drink, double capacity)
        {
            if (name == null)
                throw new ArgumentException("Name was'nt entered");
            if (name.Length < 5)
                throw new ArgumentException("Name length must be at least 5 tabs");
            Name = name;
            if (price <= 0)
                throw new NegativePriceException("price must be over 0");
            Price = price;
            if (capacity == 0)
                throw new ArgumentException("capacity can't be 0");
            Capacity = capacity;
            Food_Or_Drink = food_or_drink;
        }
        public static bool operator == (FoodItem f1, FoodItem f2)
        {
            if (f1 is null && f2 is null)
                return true;
            if (f1 is null || f2 is null)
                return false;
            return f1.Name.Equals(f2.Name);
        }
        public static bool operator !=(FoodItem f1, FoodItem f2)
        {
            return !(f1 == f2);
        }
    }
}
