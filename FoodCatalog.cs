﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaltzProj
{
    class FoodCatalog
    {
        public List<FoodItem> foodItems;
        public int Count { get; set; }
        public int FoodCount { get; set; }
        public int DrinkCount { get; set; }
        public FoodCatalog()
        {
            foodItems = new List<FoodItem>();
        }
        internal FoodItem this[string name]
        {
            get
            {
                FoodItem item = foodItems.FirstOrDefault(food => food.Name.ToUpper() == name.ToUpper());
                if (item == null)
                    throw new FoodItemNotFoundException($"Food item {name} was not found");
                return item;
            }
            set
            {
                FoodItem item = foodItems.FirstOrDefault(food => food.Name.ToUpper() == name.ToUpper());
                if (item == null)
                    throw new FoodItemNotFoundException($"Food item {name} was not found");
                item.Price = value.Price;
            }
        }
        internal List<FoodItem> this[double price]
        {
            get
            {
                List<FoodItem> copyList = new List<FoodItem>();
                foodItems.ForEach(item => copyList.Add(item));
                copyList.Sort((x1, x2) => x1.Price.CompareTo(x2.Price));
                return ListByPrice(copyList, price);
            }
        }
        private List<FoodItem> ListByPrice (List<FoodItem> list, double price)
        {
            List<FoodItem> items = new List<FoodItem>();
            list.ForEach(item =>
            {
                if (item.Price == price)
                    items.Add(item);
            });
            if (items.Count > 0)
                return items;
            FoodItem anotherItem = list.Aggregate((x, y) => Math.Abs(x.Price - price) < Math.Abs(y.Price - price) ? x : y);
            items.Add(anotherItem);
            list.Remove(anotherItem);
            list.ForEach(item =>
            {
                if (item.Price == anotherItem.Price)
                    items.Add(item);
            });
            return items;
        }
        public static FoodCatalog operator +(FoodCatalog foodCatalog, FoodItem foodItem)
        {
            foodCatalog.foodItems.Add(foodItem);
            foodCatalog.Count++;
            if (foodItem.Food_Or_Drink == true)
                foodCatalog.FoodCount++;
            else
                foodCatalog.DrinkCount++;
            return foodCatalog;
        }
        public static FoodCatalog operator -(FoodCatalog foodCatalog, FoodItem foodItem)
        {
            foodCatalog.foodItems.Remove(foodItem);
            foodCatalog.Count--;
            if (foodItem.Food_Or_Drink == true)
                foodCatalog.FoodCount--;
            else
                foodCatalog.DrinkCount--;
            return foodCatalog;
        }
    }
}
