﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaltzProj
{
    class Program
    {
        static void Main(string[] args)
        {
            FoodCatalog catalog = new FoodCatalog();
            FoodItem cola = new FoodItem("coca cola", 10, false, 3);
            FoodItem water = new FoodItem("fresh water", 5, false, 4);
            FoodItem pizza = new FoodItem("house pizza", 9, true, 6);
            FoodItem beef = new FoodItem("good beef", 12, true, 9.7);
            FoodItem pasta = new FoodItem("pasta", 45, true, 8.6);
            FoodItem orange_joice = new FoodItem("orange joice", 47, false, 12.8);
            FoodItem salat = new FoodItem("salat", 88, true, 5.1);
            FoodItem sprite = new FoodItem("sprite", 4.2, false, 4.2);
            FoodItem iceCream = new FoodItem("ice cream", 15, true, 15);
            FoodItem milk = new FoodItem("milky milk", 11, false, 19);
            catalog += cola;
            catalog += water;
            catalog += pizza;
            catalog += beef;
            catalog += pasta;
            catalog += orange_joice;
            catalog += salat;
            catalog += sprite;
            catalog += iceCream;
            catalog += milk;
            OrderBook orderBook = new OrderBook(catalog);
            Order order1 = new Order("Rotshild 11");
            order1 += cola;
            order1 += water;
            Order order2 = new Order("shapira 3");
            order2 += beef;
            order2 += pasta;
            Order order3 = new Order("king 14");
            order3 += pizza;
            order3 += orange_joice;
            Order order4 = new Order("Arlozorov 94");
            order4 += salat;
            order4 += sprite;
            Order order5 = new Order("Brodway 69");
            order5 += iceCream;
            order5 += milk;
            orderBook += order1;
            orderBook += order2;
            orderBook += order3;
            orderBook += order4;
            orderBook += order5;
            List<Order> list = orderBook.PackOnScooter(12);
            List<Order> orders = orderBook.GetExpensive(2);
        }
    }
}

