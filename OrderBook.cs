﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaltzProj
{
    class OrderBook
    {
        public List<Order> orders;
        public FoodCatalog Catalog { get; set; }
        public double TotalPrice { get; set; }
        public int TotalNumOfOrders { get; set; }
        public OrderBook(FoodCatalog catalog)
        {
            orders = new List<Order>();
            Catalog = catalog;
        }
        public static OrderBook operator +(OrderBook orderBook, Order order)
        {
            order.foodItems.ForEach(item =>
            {
                try
                {
                    FoodItem foodItem = orderBook.Catalog[item.Name];                   
                }
                catch (FoodItemNotFoundException ex)
                {
                    throw new FoodItemNotRelevantException(ex.Message + " cannot add this order to orders book");
                }
            });
            orderBook.orders.Add(order);
            orderBook.TotalNumOfOrders++;
            orderBook.TotalPrice += order.Total;
            return orderBook;
        }
        public static OrderBook operator -(OrderBook orderBook, Order order)
        {
            if (!orderBook.orders.Contains(order))
                throw new OrderNotFoundException($"{order} was not found in {orderBook}");
            orderBook.orders.Remove(order);
            orderBook.TotalNumOfOrders--;
            orderBook.TotalPrice -= order.Total;
            return orderBook;
        }
        public List<Order> GetLate()
        {
            List<Order> lateOrders = new List<Order>();
            var src = DateTime.Now;
            DateTime time = new DateTime(src.Year, src.Month, src.Day, src.Hour, 30, src.Second);
            orders.ForEach(order =>
            {
                if (order > time)
                    lateOrders.Add(order);
            });
            return lateOrders;
        }
        public List<Order> PackOnScooter (double capacity)
        {
            List<Order> orders_to_pack = new List<Order>();
            orders.ForEach(order =>
            {
                if (OrderCapacity(order.foodItems) < capacity)
                {
                    capacity -= OrderCapacity(order.foodItems);
                    orders_to_pack.Add(order);
                }
            });
            return orders_to_pack;
        }
        private double OrderCapacity(List<FoodItem> foodItems)
        {
            double sum_of_capacities = 0;
            foodItems.ForEach(item => sum_of_capacities += item.Capacity);
            return sum_of_capacities;
        }
        public List<Order> GetExpensive (int num_of_orders)
        {
            List<Order> ordersCopy = new List<Order>();
            orders.ForEach(item => ordersCopy.Add(item));
            ordersCopy.Sort((x1, x2) => x1.Total.CompareTo(x2.Total));
            ordersCopy.Reverse();
            if (orders.Count < num_of_orders)
                num_of_orders = orders.Count;
            return ordersCopy.GetRange(0, num_of_orders);
        }
        internal List<Order> this[FoodItem foodItem]
        {
            get
            {
                List<Order> orders = new List<Order>();
                orders.ForEach(order =>
                {
                    foreach (FoodItem food in order.foodItems)
                    {
                        if (food == foodItem)
                        {
                            orders.Add(order);
                            break;
                        }
                    }
                });
                return orders;
            }
        }
    }
}
